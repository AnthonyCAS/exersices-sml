(*first exercise *)
fun is_older (x1: int*int*int, x2: int*int*int)=
    if #1 x1 < #1 x2
    then true
    else false
(*second exercise *)
fun number_in_month (datos:(int*int*int) list, month:int)=
    if null datos
    then 0
    else if (#2 (hd datos)= month)
    	 then 1 + number_in_month (tl datos, month)
	 else 0 + number_in_month (tl datos, month)
(*third exercise *)
fun number_in_months (datos: (int*int*int) list, meses: int list)=
    if null meses
    then 0
    else if number_in_month(datos,hd meses)> 0
         then 1 + number_in_months(datos, tl meses)
	else number_in_months(datos, tl meses)
(*fourth exercise *)
val xmonths =["January", "February", "March", "April","May", "June", "July", "August", "September", "October", "November", "December"];
fun dates_in_month (datos: (int*int*int) list, meses: int)=
    if null datos
    then []
    else if #2(hd datos) = meses
    	 then hd datos ::  dates_in_month(tl datos, meses)
	 else dates_in_month (tl datos, meses)
(*fifth exercise *)
fun dates_in_months(datos: (int*int*int) list, months: int list)=
    if null months 
	    then []
    else dates_in_month(datos,(hd months)) @ dates_in_months(datos,(tl months))
(*sixth exersice*)
fun get_nth(lista: string list, pos: int)=
    if pos = 1
       then (hd lista)
    else get_nth(tl lista,pos-1)	   
(*seventh exersice*)	
fun date_to_string (date: (int*int*int))=
    let val h= #2 date
		 in "Fecha: " ^ (List.nth (xmonths, h-1)) ^ " " ^ (Int.toString(#3 date)) ^ ", " ^ (Int.toString(#1 date)) end
(*eight exersice*)
fun number_before_reaching_sum(sum: int, lista: int list)=
  aux_eight(0,sum,lista)


  (*aux counter*)
  fun aux_eight( sum: int,goal: int, lista: int list)=
      if (hd lista) + sum > goal
		then 0
      else 
       1+ aux_eight((hd lista) + sum,goal, (tl lista))
(*ninth exersice*)
val days_eachmonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
fun what_month (days : int) =
    if days > 0 andalso days < 366 then
     1+number_before_reaching_sum(days, days_eachmonth)
    else 0

(*tenth exersice*)
fun month_range (day_down : int, day_up : int) =
  if day_up < day_down then [] (*0*)
  else  what_month(day_down) :: month_range(day_down + 1, day_up)
(*eleventh exersice*)
fun oldest(dates: (int*int*int) list)=
    if null dates then (10000,10000,1000)(*limite*)
    else 
    let val menor = (hd dates)
        val aux = oldest(tl dates)
    in 
      if aux_menor(menor,aux) then menor else aux end
(*funcion auxiliar eleventh*)
fun aux_menor(date1: (int*int*int), date2: (int*int*int))=
    if  #1(date1)*365+ #2(date1)*30 + #3(date1) < #1(date2)*365+ #2(date2)*30 + #3(date2)
        then true
    else false
(*reto exersice twelfth*)
fun number_in_months_challenge (dates : (int*int*int) list, meses : int list) =
  if null dates orelse null meses then
    0
  else
    number_in_months(dates, duplicates_delete(meses,[]))

fun dates_in_months_challenge (dates : (int*int*int) list, meses : int list) =
  if null dates orelse null meses then []
  else  dates_in_months(dates, duplicates_delete(meses,[]))

(*aux duplicates_delete*)

fun duplicates_delete (lista: int list,aux: int list)=
    if null lista then aux
    else if check(aux,(hd lista)) then duplicates_delete(tl lista,aux)
	else duplicates_delete(tl lista,aux @ [hd lista])

fun check (lista : int list, f: int) =
  if null lista then
    false
  else if f = hd lista  then
    true
  else
    check(tl lista, f)
